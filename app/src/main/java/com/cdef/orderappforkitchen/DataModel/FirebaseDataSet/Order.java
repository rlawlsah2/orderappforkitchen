package com.cdef.orderappforkitchen.DataModel.FirebaseDataSet;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

@IgnoreExtraProperties
public class Order extends BaseData {
    //기존
//
//    public String key;
//    public String foodName;
//    public String price;
//    public String quantity;
//    public String CMDTCD;
//    public String categoryName;
//    public String categoryCode;
//    public Long time;
//

    //개편
    public int foodUid;
    public String foodName;
    public int price;
    public int quantity;
    public String IFSC_NO;
    public String IFSC_REMARK;
    public int state;
    public String time;
    public String categoryCode;
    public String categoryName;
    public String CMDTCD;
    public String key;
    ///원본 주문내역을 찾아가기 위해 추가한 데이터
    public String orderId;
    public String orderKey;
    public int isKitchen;
    public String orderedTableNo;
    public String read;


    public Order() {

    }
}

