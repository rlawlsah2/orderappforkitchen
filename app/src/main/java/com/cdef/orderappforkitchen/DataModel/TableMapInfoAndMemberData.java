package com.cdef.orderappforkitchen.DataModel;

import javax.annotation.Generated;

/**
 * Created by kimjinmo on 2016. 11. 10..
 */
@Generated("org.jsonschema2pojo")
public class TableMapInfoAndMemberData extends TableMapInfoData {

    public long men;
    public long women;

    public TableMapInfoAndMemberData(TableMapInfoData data)
    {
        this.tNo = data.tNo;
        this.height = data.height;
        this.x = data.x;
        this.y = data.y;
        this.width = data.width;
    }
    public void updateMember(long men, long women)
    {
        this.men = men;
        this.women = women;
    }

}
