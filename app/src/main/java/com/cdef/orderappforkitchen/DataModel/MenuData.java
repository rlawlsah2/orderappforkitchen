package com.cdef.orderappforkitchen.DataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import javax.annotation.Generated;

/**
 * Created by kimjinmo on 2016. 11. 10..
 */
@Generated("org.jsonschema2pojo")
public class MenuData extends BaseData {


    @SerializedName("menus")
    @Expose
    public ArrayList<MenuItemData> menus = new ArrayList<>();


    public class MenuItemData extends BaseData
    {
        public int menuUid;
        public int order;
        public int foodUid;
        public String foodImage;
        public String foodName;
        public String tag;
        public String CMDTCD;
        public int price;
        public int newFlag;
        public int hotFlag;
        public int recommendFlag;
        public int useFlag;
        public int soldOut;

    }
//
//        "menuUid": 0,
//                "order": 0,
//                "foodUid": 86,
//                "foodImage": "https://cdefi-psp.s3.amazonaws.com/menus/image_3586bebb-77fc-4998-98c6-39c017d4bda1.png",
//                "foodName": "훈제대왕칠면족",
//                "tag": "MC05",
//                "calories": 0,
//                "carbohydrate": 0,
//                "protein": 0,
//                "fat": 0,
//                "natrium": 0,
//                "description": "",
//                "price": 13000,
//                "newFlag": 0,
//                "hotFlag": 1,
//                "recommendFlag": 1,
//                "useFlag": 1
}
