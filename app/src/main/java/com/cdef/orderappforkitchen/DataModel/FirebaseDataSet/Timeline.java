package com.cdef.orderappforkitchen.DataModel.FirebaseDataSet;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

@IgnoreExtraProperties
public class Timeline extends BaseData{


    ///1. 전체채팅에서 쓰임
    public String message;
    public String sender;
    public String subject;
    public long timestamp;
    public String type;

    public String title;

    ///2. 1:1게임 걸려올때 씀.
    public boolean read;
    public String time;

    public TimelineExtra extra;

    public Timeline()
    {

    }
}

