package com.cdef.orderappforkitchen.DataModel.FirebaseDataSet;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

@IgnoreExtraProperties
public class TimelineExtra extends BaseData{

    public String branchId;
    public String cdCode;
    public String cdDivVal;
    public String codeDivision;
    public String description;
    public String foodImage;
    public String foodName;
    public String foodUid;
    public String gameHost;
    public String gameId;
    public String price;
    public String tag;
    public String type;
    public long useFlag;
    public TimelineExtra()
    {

    }
}

