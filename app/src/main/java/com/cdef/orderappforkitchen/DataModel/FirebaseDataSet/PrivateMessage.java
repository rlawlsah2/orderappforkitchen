package com.cdef.orderappforkitchen.DataModel.FirebaseDataSet;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

@IgnoreExtraProperties
public class PrivateMessage extends BaseData{

    public String time;
    public String message;
    public String date;

    public PrivateMessage()
    {

    }



}

