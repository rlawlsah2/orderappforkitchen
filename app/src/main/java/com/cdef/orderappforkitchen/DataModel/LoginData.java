package com.cdef.orderappforkitchen.DataModel;

import javax.annotation.Generated;

/**
 * Created by kimjinmo on 2016. 11. 10..
 */
@Generated("org.jsonschema2pojo")
public class LoginData extends BaseData {

    public String accessToken;
    public branchInfo branchInfo;

    public class branchInfo extends BaseData
    {
        //임시주석
        public String storeBranchUid;
        public String branchName;
        public int likes;
        public boolean isLiking;
        public univInfo univInfo;
        public String managerName;
        public String branchTel;
        public String staffUid;
        public String posIp;
        public String branchId;
        public int tablets;
        public String fbBrandName;
    }

    public class univInfo extends BaseData
    {
        public String storeBranchUid;
        public String univName;
        public int univUid;
        public double commission;
    }
}
