package com.cdef.orderappforkitchen.DataModel.FirebaseDataSet;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

@IgnoreExtraProperties
public class LastMessageList extends BaseData{


    public HashMap<String, Message> messageList;

    public LastMessageList()
    {

    }



}

