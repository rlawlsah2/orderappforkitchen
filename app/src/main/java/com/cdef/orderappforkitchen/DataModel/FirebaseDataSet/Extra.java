package com.cdef.orderappforkitchen.DataModel.FirebaseDataSet;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

@IgnoreExtraProperties

    public class Extra extends BaseData
    {
        public String ref;
        public String orderId;
        public Extra()
        {

        }
}

