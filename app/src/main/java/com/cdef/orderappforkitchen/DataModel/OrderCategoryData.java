package com.cdef.orderappforkitchen.DataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import javax.annotation.Generated;

/**
 * Created by kimjinmo on 2016. 11. 10..
 */
@Generated("org.jsonschema2pojo")
public class OrderCategoryData extends BaseData {


    @SerializedName("categories")
    @Expose
    public ArrayList<CategoryItem> categories = new ArrayList<>();


    public class CategoryItem extends BaseData
    {
        public String categoryCode;
        public String categoryName;
        public String parentCategory;
        public boolean showDisabled;
        public int storeBranchUid;

    }
}
