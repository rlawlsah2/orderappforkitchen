package com.cdef.orderappforkitchen.DataModel.FirebaseDataSet;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

@IgnoreExtraProperties
public class Notification extends BaseData{

    public String content;
    public boolean read;
    public String tableNo;
    public String time;
    public String title;
    public String type;

    public Extra extra;

    public Notification()
    {

    }

}

