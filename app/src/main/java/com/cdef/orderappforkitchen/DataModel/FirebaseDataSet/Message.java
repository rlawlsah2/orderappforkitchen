package com.cdef.orderappforkitchen.DataModel.FirebaseDataSet;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

@IgnoreExtraProperties
public class Message extends BaseData{

    public String receiver;
    public String sender;
    public String time;
    public String message;
    public String date;
    public Long timeStamp;


    public Message()
    {

    }



}

