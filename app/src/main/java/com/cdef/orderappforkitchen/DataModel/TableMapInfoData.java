package com.cdef.orderappforkitchen.DataModel;

import javax.annotation.Generated;

/**
 * Created by kimjinmo on 2016. 11. 10..
 */
@Generated("org.jsonschema2pojo")
public class TableMapInfoData extends BaseData {

    public int x;
    public int y;
    public int width;
    public int height;
    public String tNo;
}
