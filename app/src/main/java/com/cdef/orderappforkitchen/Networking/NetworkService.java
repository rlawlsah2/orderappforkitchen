package com.cdef.orderappforkitchen.Networking;

import android.support.annotation.Keep;

import com.cdef.orderappforkitchen.Networking.response.LoginResponse;

import java.util.ArrayList;

import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by ennur on 6/25/16.
 */
public interface NetworkService {

    @Keep
    @POST
    Observable<LoginResponse> LoginForManager(@Url String url,
                                              @Query("branchId") String branchId,
                                              @Query("password") String password
    );

}
