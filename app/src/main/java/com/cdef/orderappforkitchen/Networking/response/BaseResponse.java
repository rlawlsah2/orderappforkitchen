package com.cdef.orderappforkitchen.Networking.response;

import com.cdef.orderappforkitchen.DataModel.StatusData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * Created by kimjinmo on 2016. 11. 8..
 */

@Generated("org.jsonschema2pojo")
public class BaseResponse {

    public StatusData getStatus() {
        return status;
    }

    public void setStatus(StatusData status) {
        this.status = status;
    }

    @SerializedName("status")
    @Expose
    private StatusData status;
}
