package com.cdef.orderappforkitchen.Networking;



import com.cdef.orderappforkitchen.BuildConfig;
import com.cdef.orderappforkitchen.Utils.LoginUtil;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by ennur on 6/28/16.
 *
 * HTTP통신에 필요한 RETROFIT 셋팅하는 부분. 헤더에 대한 설정도 여기서 하자
 *
 */
@Module
public class NetworkModule {




    public NetworkModule() {

    }

    @Provides
    @Singleton
    Retrofit provideCall() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        // Customize the request
                        Request request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .addHeader("X-Authorization", LoginUtil.getInstance(null).getsAccessToken() != null ? LoginUtil.getInstance(null).getsAccessToken() : "")
                                .addHeader("encoding","utf-8")    //필요한 헤더를 여기서 처리해야함
                                .build();

                        okhttp3.Response response = chain.proceed(request);
                        response.cacheResponse();
                        // Customize or return the response
                        return response;
                    }
                })
                .addInterceptor(logging)
                .build();


        Retrofit tmp = new Retrofit.Builder()
                .baseUrl(BuildConfig.LoginURL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
//                .addConverterFactory(SimpleXmlConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())

                .build();


        return tmp;
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public NetworkService providesNetworkService(
            Retrofit retrofit) {
        return retrofit.create(NetworkService.class);
    }
    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public Service providesService(
            NetworkService networkService) {
        return new Service(networkService);
    }

}
