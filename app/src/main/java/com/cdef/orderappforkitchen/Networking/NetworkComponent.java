package com.cdef.orderappforkitchen.Networking;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ennur on 6/28/16.
 * NetworkModule 을 injection 하기위해 Commponent를 정의하는 클래스
 */
@Singleton
@Component(modules = {NetworkModule.class})
public interface NetworkComponent {

    ///액티비티에 적용하기
    void inject(NetworkUtil networkUtil);

}
