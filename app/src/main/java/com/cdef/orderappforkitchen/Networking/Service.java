package com.cdef.orderappforkitchen.Networking;


import android.util.Log;

import com.cdef.orderappforkitchen.BuildConfig;
import com.cdef.orderappforkitchen.Networking.response.BaseResponse;
import com.cdef.orderappforkitchen.Networking.response.LoginResponse;
import com.cdef.orderappforkitchen.Utils.ErrorUtils;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ennur on 6/25/16.
 */
public class Service {
    private final NetworkService networkService;

    public NetworkService getNetworkService() {
        return this.networkService;
    }

    public Service(NetworkService networkService) {
        this.networkService = networkService;
    }


    ////점원용 로그인
    public Observable<LoginResponse> loginForManager(String branchId, String password) {
        return this.networkService.LoginForManager(
                BuildConfig.LoginURL + "admin/api/login",
                branchId,
                password
        );
    }


    public interface GetRequestCallBack<T> {
        void onSuccess(T response);

        void onError(NetworkError networkError);

        void onRequestError(String errorCode);
    }


    public static boolean requestErrorHandler(BaseResponse response) {
        if (response != null) {
            return ErrorUtils.isSuccess(response.getStatus().code);
        } else
            return false;
    }

    private void requestErrorHandlerFORInternetCheck(BaseResponse response, GetRequestCallBack callBack) {
        if (response != null) {
            if (ErrorUtils.isSuccessForInternetCheck(response.getStatus().code)) {
                Log.d("kk9991", "API 요청 성공 : " + response.getStatus().code);
                callBack.onSuccess(response);
            } else {
                Log.d("kk9991", "API 요청 실패");
                callBack.onRequestError(response.getStatus().code);
            }

        }
    }


}
