package com.cdef.orderappforkitchen.Networking.response;

import com.cdef.orderappforkitchen.DataModel.LoginData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kimjinmo on 2016. 11. 10..
 */

public class LoginResponse extends BaseResponse {


    @SerializedName("result")
    @Expose
    public LoginData result = new LoginData();
}
