package com.cdef.orderappforkitchen.Networking;

import android.content.Context;

import com.cdef.orderappforkitchen.OrderAppForKitchenApplication;
import com.cdef.orderappforkitchen.Utils.LogUtil;

import javax.inject.Inject;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class NetworkUtil {

    @Inject
    Service service;

    private static NetworkUtil instance = null;

    public static NetworkUtil getInstance(Context context)
    {
        if(NetworkUtil.instance == null)
            instance = new NetworkUtil(context);
        return instance;
    }

    private NetworkUtil(Context context)
    {
        OrderAppForKitchenApplication
                .getNetworkComponent(context)
                .inject(this);
    }

    public Service getService()
    {
        return this.service;
    }

    public void print()
    {
        LogUtil.d("service 객체 : " + service.toString());
    }


}
