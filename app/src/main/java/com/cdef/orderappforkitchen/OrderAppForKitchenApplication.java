package com.cdef.orderappforkitchen;

import android.app.Application;
import android.content.Context;

import com.cdef.orderappforkitchen.Networking.DaggerNetworkComponent;
import com.cdef.orderappforkitchen.Networking.NetworkComponent;
import com.cdef.orderappforkitchen.Networking.NetworkModule;


/**
 * Created by kimjinmo on 2016. 11. 12..
 * 애플리케이션 전반적인 관리를 하는 클래스. 생명주기가 따로 존재함 참고바람
 */

public class OrderAppForKitchenApplication extends Application {



    public static Context mContext;
    @Override
    public void onCreate() {
        super.onCreate();



    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    public NetworkComponent networkComponent = DaggerNetworkComponent.builder()
            .networkModule(new NetworkModule())
            .build();
    public static NetworkComponent getNetworkComponent(Context context)
    {
        return ((OrderAppForKitchenApplication)context.getApplicationContext()).networkComponent;
    }

}
