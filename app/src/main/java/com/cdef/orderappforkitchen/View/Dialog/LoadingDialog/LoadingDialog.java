package com.cdef.orderappforkitchen.View.Dialog.LoadingDialog;


import android.app.ProgressDialog;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.WindowManager;

import com.cdef.orderappforkitchen.R;
import com.cdef.orderappforkitchen.Utils.Utils;
import com.cdef.orderappforkitchen.databinding.CustomProgressBinding;

/**
 * Created by Mehmet Deniz on 20/06/2017.
 *
 *
 * converted by jinmo on 31/01/2017.
 */


public class LoadingDialog extends ProgressDialog implements LifecycleOwner {

    ObservableField<String> mTitle = new ObservableField<>();
    public LoadingDialog(Context context, String title) {
        super(context, R.style.CustomDialog);
        mTitle.set(title);
    }

    public LoadingDialog(Context context, int theme, String title) {
        super(context, theme);
        mTitle.set(title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(getContext());
    }

    private void init(Context context) {
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        CustomProgressBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.custom_progress, null, false);
        setContentView(binding.getRoot());
        binding.setTitle(this.mTitle.get());

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = Utils.dpToPx(context, 300);
        params.height = Utils.dpToPx(context, 180);//WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(params);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }

    public LoadingDialog setTitle(String title)
    {
        this.mTitle.set(title);
        return this;
    }
    public void setTitleNew(String title)
    {
        this.mTitle.set(title);
    }

    @Override
    public void show() {
        super.show();
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return null;
    }
}