package com.cdef.orderappforkitchen.View.Fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.cdef.orderappforkitchen.BuildConfig;
import com.cdef.orderappforkitchen.DataModel.FirebaseDataSet.Order;
import com.cdef.orderappforkitchen.DataModel.FirebaseDataSet.OrderRoot;
import com.cdef.orderappforkitchen.R;
import com.cdef.orderappforkitchen.Utils.FBPathBuilder;
import com.cdef.orderappforkitchen.Utils.LogUtil;
import com.cdef.orderappforkitchen.Utils.LoginUtil;
import com.cdef.orderappforkitchen.View.Adapter.OrderListAdapter;
import com.cdef.orderappforkitchen.View.Dialog.LoadingDialog.LoadingDialog;
import com.cdef.orderappforkitchen.View.ViewHolder.OrderListViewHolder;
import com.cdef.orderappforkitchen.ViewModel.OrderViewModel;
import com.cdef.orderappforkitchen.databinding.FragmentMainBinding;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kimjinmo on 2018. 2. 22..
 */

public class MainFragment extends Fragment {


    FragmentMainBinding binding;
    OrderListAdapter mOrderListAdapter;

    public OrderViewModel orderViewModel;
    SoundPool sound = new SoundPool(1, AudioManager.STREAM_NOTIFICATION, 0);
    AudioManager audioManager;
    int soundID = 0;
    long initDataSize = 0;

    AlphaInAnimationAdapter alphaAdapter = null;
    GridLayoutManager manager;

    int stateCookingCount = 0;
    int stateWaitCount = 0;
    LoadingDialog loadingDialog;

    Handler handler = new Handler();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        loadingDialog = new LoadingDialog(getContext(), "처리중..");
        soundID = sound.load(getContext(), R.raw.bell, 1);
        orderViewModel = new OrderViewModel();
        orderViewModel.mProgress.observe(this, progress -> {
            switch (progress) {
                case NONE:
                    loadingDialog.dismiss();
                    break;
                case LOADING:
                    loadingDialog.show();
                    break;
                case COMPLETE:
                    Toast.makeText(getContext(), "완료되었습니다.", Toast.LENGTH_SHORT).show();
                    handler.postDelayed(() -> {
                        loadingDialog.dismiss();

                    }, 700);
                    break;
                case ERROR:
                    loadingDialog.dismiss();
                    Toast.makeText(getContext(), "일시적인 오류가 발생했습니다. 잠시후 다시시도해주세요.", Toast.LENGTH_SHORT).show();
                    break;
            }
        });

        LogUtil.d("오디오 : " + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        FirebaseDatabase.getInstance().getReference()
                .child(LoginUtil.getInstance(getContext()).getmBrandName())
                .child("orderForKitchen")
                .child(LoginUtil.getInstance(getContext()).getsBranchID())
                .orderByChild("state")
                .endAt(1)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot != null) {
                            initDataSize = dataSnapshot.getChildrenCount();
                        }
                        FirebaseDatabase.getInstance().getReference()
                                .child(LoginUtil.getInstance(getContext()).getmBrandName())
                                .child("orderForKitchen")
                                .child(LoginUtil.getInstance(getContext()).getsBranchID())
                                .orderByChild("state")
                                .endAt(1)
                                .addChildEventListener(childEventListener);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


        manager = new GridLayoutManager(getContext(), 3);
        this.binding.recyclerView.setLayoutManager(manager);
        setComplete(false);


        ///30초 단위로 화면 갱신
        Observable.interval(30, 30, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> {
                    // here is the task that should repeat
                    ///광고를 띄우기위한 조건이 무엇일까?
                    //0. 광고를 띄우고 있을땐 굳이 확인할 필요가 없지?
                    //1. 마지막 터치로부터 checkInterval만큼의 시간이 지나야 가능
                    //2. 적어도 사용하기를 누른 이후. (즉, layoutIntro 가 Visible.GONE 상태인 경우)
                    //3. 마지막 광고 띄운 이후 (=? checkinterval)
                    if (mOrderListAdapter != null) {
                        mOrderListAdapter.notifyDataSetChanged();
                    }


                });

    }

    private ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            binding.setMoreItem(mOrderListAdapter.getItemCount());

            if (initDataSize <= 0) {
                sound.play(soundID, (float) (audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION) / 15.0), (float) (audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION) / 15.0), 0, 0, 1);
            }
            initDataSize--;
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    private RecyclerView.AdapterDataObserver adapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            LogUtil.d("메뉴 adapterDataObserver : ");

        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            super.onItemRangeChanged(positionStart, itemCount);
            LogUtil.d("메뉴 onItemRangeChanged : ");

        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
            LogUtil.d("메뉴 onItemRangeInserted : ");

        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            LogUtil.d("메뉴 onItemRangeRemoved : ");

        }
    };

    public void setComplete(Boolean flag) {
        LogUtil.d("리스트 변경 : " + flag);
        /**
         * 간단한 초기화
         * */
        if (mOrderListAdapter != null) {
            mOrderListAdapter.setListener(null);
            mOrderListAdapter.unregisterAdapterDataObserver(adapterDataObserver);
        }
        mOrderListAdapter = null;
        alphaAdapter = null;
        if (flag) {
            this.binding.buttonWaiting.setTypeface(this.binding.buttonWaiting.getTypeface(), Typeface.NORMAL);
            this.binding.buttonComplete.setTypeface(this.binding.buttonComplete.getTypeface(), Typeface.BOLD);
            mOrderListAdapter = new OrderListAdapter(Order.class, R.layout.adapter_order_list, OrderListViewHolder.class,
                    FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(getContext()).getmBrandName())
                            .child("orderForKitchen")
                            .child(LoginUtil.getInstance(getContext()).getsBranchID())
                            .orderByChild("state")
                            .equalTo(2)

            );
            FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(getContext()).getmBrandName())
                    .child("orderForKitchen")
                    .child(LoginUtil.getInstance(getContext()).getsBranchID())
                    .orderByChild("state")
                    .equalTo(2).keepSynced(true);

        } else {
            this.binding.buttonWaiting.setTypeface(this.binding.buttonWaiting.getTypeface(), Typeface.BOLD);
            this.binding.buttonComplete.setTypeface(this.binding.buttonComplete.getTypeface(), Typeface.NORMAL);
            mOrderListAdapter = new OrderListAdapter(Order.class, R.layout.adapter_order_list, OrderListViewHolder.class,
                    FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(getContext()).getmBrandName())
                            .child("orderForKitchen")
                            .child(LoginUtil.getInstance(getContext()).getsBranchID())
                            .orderByChild("state")
                            .endAt(1)

            );
            FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(getContext()).getmBrandName())
                    .child("orderForKitchen")
                    .child(LoginUtil.getInstance(getContext()).getsBranchID())
                    .orderByChild("state")
                    .endAt(1).keepSynced(true);

            mOrderListAdapter.mCookingCount.observe(this, count -> this.binding.setCookingCount(count));
            mOrderListAdapter.mWaitCount.observe(this, count -> this.binding.setWaitCount(count));

        }

        this.binding.setIsCompleteList(flag);
        mOrderListAdapter.setListener(onItemClickListener);
        mOrderListAdapter.registerAdapterDataObserver(adapterDataObserver);
        alphaAdapter = new AlphaInAnimationAdapter(mOrderListAdapter);
        alphaAdapter.setInterpolator(new OvershootInterpolator());
        this.binding.recyclerView.setAdapter(alphaAdapter);

    }

    private OrderListAdapter.OnItemClickListener onItemClickListener = (v, order) -> this.orderViewModel.changeState(order);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // TODO: inflate a fragment view
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_main, container, false);

        binding.setFragment(this);
        binding.setCookingCount(0);
        binding.setWaitCount(0);
        return binding.getRoot();

    }
}
