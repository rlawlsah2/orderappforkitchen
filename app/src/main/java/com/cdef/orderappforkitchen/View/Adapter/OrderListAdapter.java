package com.cdef.orderappforkitchen.View.Adapter;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.MutableLiveData;
import android.databinding.DataBindingUtil;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.cdef.orderappforkitchen.DataModel.FirebaseDataSet.Order;
import com.cdef.orderappforkitchen.Utils.LogUtil;

import com.cdef.orderappforkitchen.View.ViewHolder.OrderListViewHolder;
import com.cdef.orderappforkitchen.databinding.AdapterOrderListBinding;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.ObservableSnapshotArray;
import com.firebase.ui.database.SnapshotParser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Query;

import java.util.Currency;
import java.util.Locale;

/**
 * Created by kimjinmo on 2018. 2. 22..
 */


public class OrderListAdapter extends FirebaseRecyclerAdapter<Order, OrderListViewHolder> {

    public static OrderListAdapter.OnItemClickListener listener;

    public MutableLiveData<Integer> mCookingCount = new MutableLiveData<>();
    public MutableLiveData<Integer> mWaitCount = new MutableLiveData<>();

    public void setListener(OrderListAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }


    public OrderListAdapter(Class<Order> modelClass, @LayoutRes int modelLayout, Class<OrderListViewHolder> viewHolderClass, Query query) {
        super(modelClass, modelLayout, viewHolderClass, query);
    }

    public ObservableSnapshotArray<Order> getData() {
        return this.mSnapshots;
    }


    @Override
    public void onDataChanged() {
        super.onDataChanged();
        calcCount();
    }

    public void calcCount()
    {
        int cookingCount = 0;
        int waitCount = 0;
        for(DataSnapshot item : this.mSnapshots)
        {
            if(item.getValue(Order.class).state == 1)
            {
                waitCount ++;
            }
            else if(item.getValue(Order.class).state == 0)
            {
                cookingCount ++;
            }
        }

        this.mCookingCount.setValue(cookingCount);
        this.mWaitCount.setValue(waitCount);
    }

    @Override
    protected void populateViewHolder(OrderListViewHolder holder, Order dataSet, int position) {
        LogUtil.d("리스트 구성중 : " + dataSet.foodName);

        holder.binding.setOrder(dataSet);
        holder.binding.setItemClickListener(listener);
    }


    public void onDestroy() {


    }

    public interface OnItemClickListener {
        void onOrderItemClick(View v, Order order);

    }


}
