package com.cdef.orderappforkitchen.View.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.cdef.orderappforkitchen.R;
import com.cdef.orderappforkitchen.Utils.LogUtil;
import com.cdef.orderappforkitchen.databinding.DialogMessageviewBinding;
/**
 * Created by kimjinmo on 2017. 1. 2..
 */

public class MessageDialogBuilder extends Dialog {

    ////멤버변수
    private Context mContext;
    private static MessageDialogBuilder mInstance;
    ////

    DialogMessageviewBinding binding;

    public static MessageDialogBuilder getInstance(Context context) {
        if (MessageDialogBuilder.mInstance != null) {
            MessageDialogBuilder.mInstance.dismiss();
        }

        if (MessageDialogBuilder.mInstance == null) {
            MessageDialogBuilder.mInstance = new MessageDialogBuilder(context);
        }

        MessageDialogBuilder.mInstance.init(context);
        return MessageDialogBuilder.mInstance;
    }

    public static void clearInstance() {
        MessageDialogBuilder.mInstance = null;
    }



    private MessageDialogBuilder(Context context) {
        super(context);
//        this.mContext = context;
        this.mContext = context;
        registerHandler();
    }

    private MessageDialogBuilder(Context context, int themeResId) {
        super(context, themeResId);
//        this.mContext = context;
        registerHandler();
    }

    private MessageDialogBuilder(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
//        this.mContext = context;
        registerHandler();
    }

    /**
     * 초기 뷰 셋팅을 위한 부분
     **/
    private void registerHandler() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        this.binding  = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_messageview, null, false);
        setContentView(binding.getRoot());




//        setContentView(R.layout.dialog_messageview);
//        this.unbinder = ButterKnife.bind(this);


        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        ///1. 윈도우 셋팅
//        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
//        WindowManager.LayoutParams params = this.getWindow().getAttributes();
//        int calcWidth = (int) (displayMetrics.widthPixels * 0.9);
//        params.width = (calcWidth > Utils.dpToPx(mContext, 320) ? calcWidth : Utils.dpToPx(mContext, 320)); //(int) (displayMetrics.widthPixels * 0.88);//Utils.dpToPx(mContext, );//LinearLayout.LayoutParams.MATCH_PARENT;
//        this.getWindow().setAttributes((WindowManager.LayoutParams) params);

        ///2. 하위 뷰 셋팅
        Window window = this.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCanceledOnTouchOutside(false);

    }

     public MessageDialogBuilder setOnDismissListener_(@Nullable OnDismissListener listener) {
        super.setOnDismissListener(listener);
        return this;
    }

    public MessageDialogBuilder init(Context context) {
        ///클린에 대한 메소드가 들어가야함

        if (this.isShowing())
            this.dismiss();
        this.mContext = context;
        this.binding.AlertTitle.setText("알림");
        this.binding.AlertContent.setText("내용");
        this.binding.AlertCANCLE.setText("취소");
        this.binding.AlertCANCLE.setVisibility(View.GONE);
        this.binding.AlertOK.setText("확인");
        this.binding.AlertOK.setVisibility(View.GONE);

        return this;
    }

    @Override
    public void dismiss() {
        super.dismiss(); 
        MessageDialogBuilder.mInstance = null;
    }

    public MessageDialogBuilder setTitle(String title) {
        this.binding.AlertTitle.setText(title);
        return this;
    }

    public MessageDialogBuilder setContent(String content) {
        this.binding.AlertContent.setText(content);
        return this;
    }

    public MessageDialogBuilder setContent(Spanned content) {
        this.binding.AlertContent.setText(content);
        return this;
    }

    public MessageDialogBuilder setCancelButton(String title, View.OnClickListener listener) {

        if (title != null)
            this.binding.AlertCANCLE.setText(title);
        this.binding.AlertCANCLE.setOnClickListener(listener);
        this.binding.AlertCANCLE.setVisibility(View.VISIBLE);
        return this;
    }

    public MessageDialogBuilder setCancelButton() {

        this.binding.AlertCANCLE.setText("취소");
        this.binding.AlertCANCLE.setOnClickListener(view -> {
            this.dismiss();
        });
        this.binding.AlertCANCLE.setVisibility(View.VISIBLE);
        return this;
    }

    public MessageDialogBuilder setOkButton(String title, View.OnClickListener listener) {

        if (title != null)
            this.binding.AlertOK.setText(title);
        this.binding.AlertOK.setOnClickListener(listener);
        this.binding.AlertOK.setVisibility(View.VISIBLE);

        return this;
    }


    public MessageDialogBuilder setDefaultButton() {
        this.binding.AlertOK.setOnClickListener(view -> dismiss());
        this.binding.AlertOK.setVisibility(View.VISIBLE);
        this.binding.viewDivider.setVisibility(View.GONE);
        return this;
    }

    /***
     * 취소버튼은 기본, 확인버튼만 람다로
     ***/
    public MessageDialogBuilder setDefaultButtons(View.OnClickListener listener) {
        this.binding.AlertCANCLE.setOnClickListener(view -> dismiss());
        this.binding.AlertCANCLE.setVisibility(View.VISIBLE);
        this.binding.AlertOK.setOnClickListener(listener);
        this.binding.AlertOK.setVisibility(View.VISIBLE);
        this.binding.viewDivider.setVisibility(View.VISIBLE);

        return this;
    }

    public void complete() {
        LogUtil.d("MessageDialogBuilder 에서 complete : " + !((Activity) this.mContext).isFinishing());
        if (!((Activity) this.mContext).isFinishing())
            this.show();
    }


}
