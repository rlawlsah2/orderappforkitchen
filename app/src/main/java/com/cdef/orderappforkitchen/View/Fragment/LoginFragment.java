package com.cdef.orderappforkitchen.View.Fragment;


import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.cdef.orderappforkitchen.R;
import com.cdef.orderappforkitchen.Utils.LogUtil;
import com.cdef.orderappforkitchen.Utils.LoginUtil;
import com.cdef.orderappforkitchen.View.Dialog.LoadingDialog.LoadingDialog;
import com.cdef.orderappforkitchen.View.Dialog.MessageDialogBuilder;
import com.cdef.orderappforkitchen.View.MainActivity;
import com.cdef.orderappforkitchen.ViewModel.LoginViewModel;
import com.cdef.orderappforkitchen.databinding.FragmentLoginBinding;


/**
 * Created by kimjinmo on 2017. 9. 18..
 */

public class LoginFragment extends BaseFragment {

    LoginViewModel loginViewModel;
    FragmentLoginBinding binding = null;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        loadingDialog = new LoadingDialog(getContext(), getString(R.string.loadingDialog_title_login));
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        loginViewModel.initRepository(getContext());
        loginViewModel.loginInfo.observe(this, loginData -> {
            LogUtil.d("livedata observe 테스트 입니다1 : " + loginData);
            if (loginData != null)   //로그인 성공
            {
                loginViewModel.saveDefaultLoginInfo(getContext());
            } else {
                ///로그인 실패
                Toast.makeText(getContext(), "로그인 실패\n입력된 정보를 확인해주세요.", Toast.LENGTH_SHORT).show();
            }
        });
        loginViewModel.initSetting.observe(this, initsetting -> {
            switch (initsetting)
            {
                case NONE:
                    this.loadingDialog.dismiss();
                    LogUtil.d("프래그먼트에서 보는 초기화 셋팅 스트림 현황 NONE");
                    this.binding.buttonLogin.setText(R.string.button_login_default);
                    break;

                case LOADING:
                    this.loadingDialog.show();
                    LogUtil.d("프래그먼트에서 보는 초기화 셋팅 스트림 현황 LOADING");
                    this.binding.buttonLogin.setText(R.string.button_login_doing);
                    break;

                case ERROR:
                    this.loadingDialog.dismiss();
                    this.binding.buttonLogin.setText(R.string.button_login_fail);
                    LogUtil.d("프래그먼트에서 보는 초기화 셋팅 스트림 현황 ERROR");
                    MessageDialogBuilder.getInstance(getContext())
                            .setTitle("알림")
                            .setContent("초기화 실패\n다시 시도할까요?")
                            .setOkButton("재시도", v -> {
                                binding.buttonLogin.performClick();
                            })
                            .setCancelButton()
                            .complete();
                    break;

                case COMPLETE:
                    LogUtil.d("프래그먼트에서 보는 초기화 셋팅 스트림 현황 COMPLETE");
                    this.loadingDialog.dismiss();
                    this.binding.buttonLogin.setText(R.string.button_login_success);
                    clickOUT(this.binding.editPW);

                    new Handler().postDelayed(() -> ((MainActivity) getActivity()).goMain(), 1000);
                    break;
            }
        });
        binding.setLoginViewModel(this.loginViewModel);

        /**
         * 입력창 이벤트 달기
         * **/
        this.binding.editID.setOnFocusChangeListener((view, b) -> {
            LogUtil.d("아이디 포커스 : " + b);
            this.binding.setFocusID(b);
        });
        this.binding.editPW.setOnFocusChangeListener((view, b) -> {
            LogUtil.d("패스워드 포커스 : " + b);
            this.binding.setFocusPW(b);
        });


        ////패스워드에서 완료 누르면 바로 진행되게끔
        this.binding.editPW.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                    case KeyEvent.KEYCODE_ENTER:
                        binding.buttonLogin.performClick();
                        return true;
                    default:
                        break;
                }
            }
            return false;
        });

        ////로컬에 저장된 데이터가 있는지 체크
        if (LoginUtil.getInstance(getContext()).isVaild()) {
            this.binding.editID.setText(LoginUtil.getInstance(getContext()).getsStaffID());
            this.binding.editPW.setText(LoginUtil.getInstance(getContext()).getsStaffPW());
            this.binding.buttonLogin.setText("자동 로그인중..");
            (new Handler()).postDelayed(() -> binding.buttonLogin.performClick(), 1000);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_login, container, false);
        binding.setFragment(this);
        return binding.getRoot();
    }


    public void goLogin(View view) {

        LogUtil.d("goLogin을 눌렀음");
        String ID = this.binding.editID.getText().toString();
        String PW = this.binding.editPW.getText().toString();

        if ((ID != null && PW != null) && (ID.length() > 0 && PW.length() > 0)) {
            ID = ID.toUpperCase();
            this.binding.editID.setText(ID);
            this.loginViewModel.getLoginInfo(ID, PW);
        } else {
            Toast.makeText(getContext(), "아이디 패스워드 테이블 번호를 입력 후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 바탕화면 눌렀을때 키 입력창 사라지게
     * **/
    public void clickOUT(View v) {
        if(getActivity().getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) (getActivity().getSystemService(Context.INPUT_METHOD_SERVICE));
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @BindingAdapter("android:layout_height")
    public static void setLayoutHeight(View view, float height) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = (int) height;
        view.setLayoutParams(layoutParams);
    }
}
