package com.cdef.orderappforkitchen.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.cdef.orderappforkitchen.R;
import com.cdef.orderappforkitchen.Utils.DefaultExceptionHandler;
import com.cdef.orderappforkitchen.Utils.NavigationUtils;
import com.cdef.orderappforkitchen.View.Fragment.LoginFragment;
import com.cdef.orderappforkitchen.View.Fragment.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));



        NavigationUtils.detailOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, LoginFragment.class.getSimpleName());

//        NavigationUtils.detailOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, MainFragment.class.getSimpleName());
    }


    public void goMain() {
        NavigationUtils.detailOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, MainFragment.class.getSimpleName());

        //포스에 넣을 수 있는 상태인지 체크

    }
}
