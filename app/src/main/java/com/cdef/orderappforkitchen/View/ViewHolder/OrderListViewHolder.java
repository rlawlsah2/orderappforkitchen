package com.cdef.orderappforkitchen.View.ViewHolder;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.cdef.orderappforkitchen.databinding.AdapterOrderListBinding;

/**
 * Created by kimjinmo on 2018. 3. 14..
 */

public class OrderListViewHolder extends RecyclerView.ViewHolder  {


    public AdapterOrderListBinding binding;

    public OrderListViewHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }
}
