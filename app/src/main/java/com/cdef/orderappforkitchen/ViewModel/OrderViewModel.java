package com.cdef.orderappforkitchen.ViewModel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.cdef.orderappforkitchen.BuildConfig;
import com.cdef.orderappforkitchen.DataModel.FirebaseDataSet.Order;
import com.cdef.orderappforkitchen.Utils.FBPathBuilder;
import com.cdef.orderappforkitchen.Utils.LogUtil;
import com.cdef.orderappforkitchen.Utils.LoginUtil;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kimjinmo on 2018. 2. 27..
 */

public class OrderViewModel extends ViewModel {
    public OrderViewModel() {
        this.mProgress.setValue(PROFRESS.NONE);

    }

    public MutableLiveData<PROFRESS> mProgress = new MutableLiveData<>();

    public enum PROFRESS {
        NONE, LOADING, ERROR, COMPLETE
    }

    public Map<String, Object> clickedItem = new HashMap<>();

    public void changeState(Order order) {

        LogUtil.d("선택한 아이템 : " + order.foodName);

        this.mProgress.setValue(PROFRESS.LOADING);
        this.clickedItem.clear();

        Map<String, Object> proccess = new HashMap<>();    //주문을 담을 객체
        ///1. orderData쪽 업데이트
        proccess.put(
                FBPathBuilder.getInstance()
                        .init()
                        .set(LoginUtil.getInstance(null).getmBrandName())
                        .set("orderData")
                        .set(LoginUtil.getInstance(null).getsBranchID())
                        .set(order.orderId)
                        .set("orders")
                        .set(order.orderKey)
                        .set("state")
                        .complete()
                , ((order.state == 1) ? 0 : (order.state == 0 ? 2 : 1))

        );

        ///2. orderForKitchen쪽
        proccess.put(
                FBPathBuilder.getInstance()
                        .init()
                        .set(LoginUtil.getInstance(null).getmBrandName())
                        .set("orderForKitchen")
                        .set(LoginUtil.getInstance(null).getsBranchID())
                        .set(order.orderKey)
                        .set("state")
                        .complete()
                , ((order.state == 1) ? 0 : (order.state == 0 ? 2 : 1))
        );

        ///3. orderForKitchenComplete
        if (order.state == 0)    //조리중 -> 조리완료로 변할시점
        {
            order.state = 2;
            order.read = null;
            proccess.put(
                    FBPathBuilder.getInstance()
                            .init()
                            .set(LoginUtil.getInstance(null).getmBrandName())
                            .set("orderForKitchenComplete")
                            .set(LoginUtil.getInstance(null).getsBranchID())
                            .set(order.orderKey)
                            .complete()
                    , order
            );
        }

        FirebaseDatabase.getInstance().getReference()
                .updateChildren(proccess, (databaseError, databaseReference) -> {
                    if (databaseError == null) {
                        this.mProgress.setValue(PROFRESS.COMPLETE);
                    } else {
                        this.mProgress.setValue(PROFRESS.ERROR);
                    }
                });
    }
}
