package com.cdef.orderappforkitchen.ViewModel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.cdef.orderappforkitchen.DataModel.LoginData;
import com.cdef.orderappforkitchen.Networking.NetworkUtil;
import com.cdef.orderappforkitchen.Repository.LoginRepository;
import com.cdef.orderappforkitchen.Utils.LogUtil;
import com.cdef.orderappforkitchen.Utils.LoginUtil;


/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class LoginViewModel extends ViewModel {


    public MutableLiveData<LoginData> loginInfo = new MutableLiveData<LoginData>() {
    };
    public MutableLiveData<INITSETTING> initSetting = new MutableLiveData<INITSETTING>() {
    };
    private LoginRepository loginRepository;

    private String mStaffID = null;
    private String mStaffPW = null;

    public enum INITSETTING {
        NONE, LOADING, ERROR, COMPLETE
    }

    public LoginViewModel() {

    }

    public void initRepository(Context context) {
        this.loginRepository = new LoginRepository(NetworkUtil.getInstance(context).getService());
        this.initSetting.setValue(INITSETTING.NONE);

    }

    public void getLoginInfo(String branchId, String pw) {
        this.initSetting.setValue(INITSETTING.LOADING);
        loginRepository.getLoginInfo(branchId, pw)
                .subscribe(
                        response -> {
                            LogUtil.d("로그인 modelView response : " + response.getValue());

                            mStaffID = branchId;
                            mStaffPW = pw;
                            loginInfo.setValue(response.getValue());

                        },
                        error -> {
                            LogUtil.d("로그인 modelView error : " + error);
                            loginInfo.setValue(null);
                            this.initSetting.setValue(INITSETTING.ERROR);

                        },
                        () -> {

                            LogUtil.d("로그인 modelView complete : ");
                        }
                );
    }

    public void saveDefaultLoginInfo(Context context) {
        LogUtil.d("로그인 성공 이후 로컬 저장 saveDefaultLoginInfo : " + mStaffID);
        LogUtil.d("로그인 성공 이후 로컬 저장 saveDefaultLoginInfo : " + mStaffPW);

        if (this.mStaffID != null && this.mStaffPW != null) {
            LoginUtil.getInstance(context).setLoginInfo(context, this.loginInfo.getValue().branchInfo.fbBrandName, this.mStaffID, this.mStaffPW, this.loginInfo.getValue().branchInfo.branchId, this.loginInfo.getValue().branchInfo.storeBranchUid, this.loginInfo.getValue().accessToken, this.loginInfo.getValue().branchInfo.posIp);
            this.initSetting.setValue(INITSETTING.COMPLETE);

        }


    }
}
