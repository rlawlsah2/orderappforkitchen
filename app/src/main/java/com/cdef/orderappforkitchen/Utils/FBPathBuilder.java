package com.cdef.orderappforkitchen.Utils;

/**
 * Created by kimjinmo on 2018. 2. 23..
 */

public class FBPathBuilder {


    private StringBuilder builder;
    private static FBPathBuilder instance = null;

    private FBPathBuilder()
    {
        builder = new StringBuilder();
    }

    public static FBPathBuilder getInstance()
    {

        if(instance == null)
            instance = new FBPathBuilder();
        return FBPathBuilder.instance;
    }

    public FBPathBuilder init()
    {
        this.builder.setLength(0);
        this.builder.append("/");
        return this;
    }

    public FBPathBuilder set(String path)
    {
        this.builder.append(path);
        this.builder.append("/");
        return this;
    }

    public String complete()
    {
        return this.builder.toString();
    }


}
