package com.cdef.orderappforkitchen.Utils;

import com.cdef.orderappforkitchen.DataModel.FirebaseDataSet.Order;
import com.cdef.orderappforkitchen.DataModel.OrderCategoryData;
import com.cdef.orderappforkitchen.DataModel.TableMapInfoData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by kimjinmo on 2017. 9. 20..
 * 태블릿 로그인시 미리 준비할 데이터를 포함한다.
 * 1. 주문 카테고리 메뉴 갱신
 * 2. 주문 서브 메뉴 갱신
 * 3. 테이블 맵 정보 갱신하기
 * 4. 게임 목록 갱신하기
 */

public class TabletSetting {


    public ArrayList<OrderCategoryData.CategoryItem> mOrderMainCategoryList;
    public HashMap<String, ArrayList<OrderCategoryData.CategoryItem>> mOrderSubCategoryList;
    public ArrayList<TableMapInfoData> mTableMapInfo;
    private ArrayList<OrderCategoryData.CategoryItem> mOrderSubCategoryAllList;
    private HashMap<String, OrderCategoryData.CategoryItem> mOrderSubCategoryAllHashMap;


    ///내 테이블 카트에 담긴 정보
    public HashMap<String, Order> mCartList;

    private static TabletSetting instance;


    private TabletSetting()
    {
        this.mOrderMainCategoryList = new ArrayList<>();
        this.mOrderSubCategoryList = new HashMap<>();
        this.mTableMapInfo = new ArrayList<>();
        this.mOrderSubCategoryAllList = new ArrayList<>();
        this.mOrderSubCategoryAllHashMap = new HashMap<>();
        this.mCartList = new HashMap<>();
    }

    public ArrayList<String> getTableMapAsString()
    {
        ArrayList<String> list = new ArrayList<>();
        for(TableMapInfoData item : this.mTableMapInfo)
        {
            list.add(item.tNo);
        }
        return list;
    }

    public ArrayList<OrderCategoryData.CategoryItem> getmOrderSubCategoryAllList()
    {
        if(mOrderSubCategoryAllList.size() == 0)
        {

            for(String key : this.mOrderSubCategoryList.keySet())
            {
                for(OrderCategoryData.CategoryItem item : this.mOrderSubCategoryList.get(key))
                {
                    this.mOrderSubCategoryAllList.add(item);
                }
            }
        }

        return mOrderSubCategoryAllList;
    }



    public HashMap<String, OrderCategoryData.CategoryItem> getmOrderSubCategoryAllHashMap()
    {
        if(mOrderSubCategoryAllHashMap.size() == 0)
        {

            for(String key : this.mOrderSubCategoryList.keySet())
            {
                for(OrderCategoryData.CategoryItem item : this.mOrderSubCategoryList.get(key))
                {
                    this.mOrderSubCategoryAllHashMap.put(item.categoryCode, item);
                }
            }
        }

        return mOrderSubCategoryAllHashMap;
    }




    /**
     * @param subCategoryId 서브에 있는 카테고리 코드
     * **/
    public String getMainCategoryName(String subCategoryId)
    {
        Iterator<OrderCategoryData.CategoryItem> itr = mOrderMainCategoryList.iterator();
        while (itr.hasNext()) {
            OrderCategoryData.CategoryItem cur = itr.next();
            LogUtil.d("getMainCategoryName 검색1 : " + cur.categoryName);

            for(OrderCategoryData.CategoryItem item : mOrderSubCategoryList.get(cur.categoryCode))
            {

                LogUtil.d("getMainCategoryName 검색2 : " + item.categoryName);
                if(item != null && item.categoryCode != null && item.categoryCode.equals(subCategoryId))
                {
                    return cur.categoryName;
                }
            }
        }
        return null;
    }


    public static synchronized TabletSetting getInstance()
    {
        if(instance == null)
            instance = new TabletSetting();

        return instance;
    }

    public void setOrderMainCategoryList(ArrayList<OrderCategoryData.CategoryItem> list)
    {
        if(this.mOrderMainCategoryList != null)
            this.mOrderMainCategoryList.clear();
        this.mOrderMainCategoryList.addAll(list);
    }
    public void setOrderSubCategoryList(String categoryId, ArrayList<OrderCategoryData.CategoryItem> list)
    {
        if(this.mOrderSubCategoryList != null)
        {
            this.mOrderSubCategoryList.put(categoryId, list);

            LogUtil.d("처음 셋팅  " + categoryId + "의 값은 : " + list.size());
        }
    }


    public void setmTableMapInfo(ArrayList<TableMapInfoData> list)
    {
        if(this.mTableMapInfo != null)
            this.mTableMapInfo.clear();
        this.mTableMapInfo.addAll(list);
    }


}
