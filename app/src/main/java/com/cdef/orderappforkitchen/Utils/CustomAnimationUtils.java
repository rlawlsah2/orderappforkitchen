package com.cdef.orderappforkitchen.Utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;

/**
 * Created by kimjinmo on 2017. 4. 3..
 */

public class CustomAnimationUtils {
    public static boolean isRunning = false;

    public static void resizeHeight(View view, int currentHeight, int resizeHeight)
    {

        if(view.getHeight() != resizeHeight)
        {
            ValueAnimator va = ValueAnimator.ofInt(currentHeight, resizeHeight);
            va.addUpdateListener(valueAnimator -> {
//                view.setAlpha((view.getHeight() > resizeHeight ? 0 : 1));
                Integer value = (Integer) valueAnimator.getAnimatedValue();
                view.getLayoutParams().height = value.intValue();
                view.requestLayout();
            });
//
            ObjectAnimator oa = ObjectAnimator.ofFloat(view, "alpha", (view.getHeight() > resizeHeight ? 0 : 1));
            oa.start();

            AnimatorSet set = new AnimatorSet();
            set.playTogether(va, oa);
            set.setDuration(250);
            set.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    CustomAnimationUtils.isRunning = true;
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    CustomAnimationUtils.isRunning = false;

                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            set.start();

        }
    }

    public static void moveRightToLeft(View view)
    {
        view.setVisibility(View.VISIBLE);
        int viewSize = view.getLayoutParams().width;
        if(viewSize != 0)
        {
            ValueAnimator va = ValueAnimator.ofInt((int)view.getX(), (int)(view.getX()-viewSize));
            va.addUpdateListener(valueAnimator -> {
//                view.setAlpha((view.getHeight() > resizeHeight ? 0 : 1));
                Integer value = (Integer) valueAnimator.getAnimatedValue();
                view.setX(value.floatValue());
                view.requestLayout();
            });
//
//            ObjectAnimator oa = ObjectAnimator.ofFloat(view, "alpha", (view.getHeight() > resizeHeight ? 0 : 1));
//            oa.start();

            AnimatorSet set = new AnimatorSet();
            set.playTogether(va);
//            set.playTogether(va, oa);
            set.setDuration(200);

            set.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    CustomAnimationUtils.isRunning = true;
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    CustomAnimationUtils.isRunning = false;

                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            set.start();
        }
    }

    public static void moveLeftToRight(View view)
    {
//        view.setVisibility(View.GONE);

        int viewSize = view.getLayoutParams().width;
        if(viewSize != 0)
        {
            ValueAnimator va = ValueAnimator.ofInt((int)view.getX(), (int)(view.getX()+viewSize));
            va.addUpdateListener(valueAnimator -> {
//                view.setAlpha((view.getHeight() > resizeHeight ? 0 : 1));
                Integer value = (Integer) valueAnimator.getAnimatedValue();
                view.setX(value.floatValue());
                view.requestLayout();
            });
//
//            ObjectAnimator oa = ObjectAnimator.ofFloat(view, "alpha", (view.getHeight() > resizeHeight ? 0 : 1));
//            oa.start();

            AnimatorSet set = new AnimatorSet();
            set.playTogether(va);
//            set.playTogether(va, oa);
            set.setDuration(200);

            set.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    CustomAnimationUtils.isRunning = true;
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    CustomAnimationUtils.isRunning = false;

                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            set.start();        }
    }
    public static void keyboardOn(View view)
    {

        ValueAnimator va = ValueAnimator.ofInt((int)view.getY(), (int)(view.getY()-400));
        va.addUpdateListener(valueAnimator -> {
            Integer value = (Integer) valueAnimator.getAnimatedValue();
            view.setY(value.floatValue());
            view.requestLayout();
        });

        AnimatorSet set = new AnimatorSet();
        set.playTogether(va);
        set.setDuration(200);

        set.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                CustomAnimationUtils.isRunning = true;
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                CustomAnimationUtils.isRunning = false;

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        set.start();        }
    public static void keyboardOff(View view)
    {

        ValueAnimator va = ValueAnimator.ofInt((int)view.getY(), 0);
        va.addUpdateListener(valueAnimator -> {
            Integer value = (Integer) valueAnimator.getAnimatedValue();
            view.setY(value.floatValue());
            view.requestLayout();
        });

        AnimatorSet set = new AnimatorSet();
        set.playTogether(va);
        set.setDuration(200);

        set.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                CustomAnimationUtils.isRunning = true;
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                CustomAnimationUtils.isRunning = false;

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        set.start();        }

}
