package com.cdef.orderappforkitchen.Utils;

import android.content.Context;
import android.databinding.ObservableField;

/**
 * Created by kimjinmo on 2017. 9. 20..
 * 태블릿 로그인 정보를 저장
 */

public class LoginUtil {

    public String getBTModuleName() {
        return "DreamEn";
    }

    public String getsStaffID() {
        return sStaffID;
    }

    public String getsStaffPW() {
        return sStaffPW;
    }

    public String getsBranchUid() {
        return sBranchUid;
    }

    public String getsAccessToken() {
        return sAccessToken;
    }

    public String getsPosIp() {
        return sPosIp;
    }

    ////필요한 로그인 정보를 static으로 가지고 있는다.
    private String sStaffID;

    public String getsBranchID() {


        LogUtil.d("지점아이디 불러올때 : " + this.sBranchID);
        return sBranchID;
    }

    public void setsBranchID(String sBranchID) {
        this.sBranchID = sBranchID;
    }

    private String sBranchID;
    private String sStaffPW;
    private String sBranchUid;
    private String sAccessToken;
    private String sPosIp;

    public String getmBrandName() {

        LogUtil.d("브랜드명 불러올때 : " + this.mBrandName);
        return mBrandName;
    }

    public void setmBrandName(String mBrandName) {
        this.mBrandName = mBrandName;
    }

    private String mBrandName = null;
    public String getmSelectedTableNo() {
        return mSelectedTableNo.get();
    }

    public void setmSelectedTableNo(String mSelectedTableNo) {

        this.preSelectedTableNo = this.mSelectedTableNo.get();
        this.mSelectedTableNo.set(mSelectedTableNo);
    }



    public ObservableField<String> mSelectedTableNo = new ObservableField<>();

    public String getPreSelectedTableNo() {
        return preSelectedTableNo;
    }

    public void setPreSelectedTableNo(String preSelectedTableNo) {
        this.preSelectedTableNo = preSelectedTableNo;
    }

    private String preSelectedTableNo;

    public String getmOrderSTBL_CD() {
        return mOrderSTBL_CD;
    }

    public void setmOrderSTBL_CD(String mOrderSTBL_CD) {
        this.mOrderSTBL_CD = mOrderSTBL_CD;
    }

    private String mOrderSTBL_CD;

    public String getmOrderID() {
        return mOrderID;
    }

    public void setmOrderID(String mOrderID) {
        this.mOrderID = mOrderID;
    }

    private String mOrderID;

    public int getmOrderIFSC_REMARK() {
        return mOrderIFSC_REMARK;
    }

    public void setmOrderIFSC_REMARK(int mOrderIFSC_REMARK) {
        this.mOrderIFSC_REMARK = mOrderIFSC_REMARK;
    }

    private int mOrderIFSC_REMARK;

    private static LoginUtil instance;


    private LoginUtil(Context context) {
        sStaffID = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_ID);
        sBranchUid = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_STOREBRANCHUID);
        sBranchID = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_STOREBRANCHID);
        sStaffPW = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_PW);
        sAccessToken = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_ACCESSTOKEN);
        sPosIp = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_POSIP);
    }

    public void setLoginInfo(Context context, String brandName, String id, String pw, String branchID, String branchUid, String accessToken, String posIp) {


        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_ID, id);
        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_PW, pw);
        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_ACCESSTOKEN, accessToken);
        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_STOREBRANCHID, branchID);
        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_STOREBRANCHUID, branchUid);
        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_POSIP, posIp);

        sStaffID = id;
        sStaffPW = pw;
        sAccessToken = accessToken;
        sBranchUid = branchUid;
        sBranchID = branchID;
        sPosIp = posIp;
        this.mBrandName = brandName;
    }

    /**
     * 무결성 검사. 5가지 변수값에 이상이 없는지 확인한다. 이상있을경우 false
     **/
    public boolean isVaild() {
        if ((sStaffID != null && sStaffID.length() > 0) &&
                (sStaffPW != null && sStaffPW.length() > 0) &&
                (sAccessToken != null && sAccessToken.length() > 0) &&
                (sBranchUid != null && sBranchUid.length() > 0) &&
                (sBranchID != null && sBranchID.length() > 0) &&
                (sPosIp != null && sPosIp.length() > 0)) {
            return true;
        } else
            return false;
    }

    public static LoginUtil getInstance(Context context) {
        if (context == null && instance != null)
            return instance;

        if (instance == null)
            instance = new LoginUtil(context);
        return instance;
    }


}
