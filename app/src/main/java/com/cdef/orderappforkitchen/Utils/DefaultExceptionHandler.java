package com.cdef.orderappforkitchen.Utils;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.cdef.orderappforkitchen.View.MainActivity;

import java.lang.Thread.UncaughtExceptionHandler;

/**
 * This custom class is used to handle exception.
 *
 * @author Chintan Rathod (http://www.chintanrathod.com)
 */
public class DefaultExceptionHandler implements UncaughtExceptionHandler {


    private Activity activity;

    public DefaultExceptionHandler(Activity a) {
        activity = a;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        LogUtil.d("에러가 터졌네여 1: " + ex);

        Intent intent = new Intent(activity, MainActivity.class);
        intent.putExtra("crash", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        LogUtil.d("에러가 터졌네여 2: " + ex);

        PendingIntent pendingIntent = PendingIntent.getActivity(activity, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        LogUtil.d("에러가 터졌네여 3: " + ex);

        AlarmManager mgr = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
        LogUtil.d("에러가 터졌네여 4: " + ex);

        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 300, pendingIntent);
        LogUtil.d("에러가 터졌네여 5: " + ex);

        activity.finish();
        System.exit(2);
    }
}