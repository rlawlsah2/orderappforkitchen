package com.cdef.orderappforkitchen.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


/*
*   Preference 기능에 대해 null일 경우에 대해 처리한 클래스
*
* */

public class PreferenceUtils
{
    public static SharedPreferences getPreferences(Context context, String prefName)
    {
        SharedPreferences preference = null;
        if ((context == null) || (prefName == null) || ("".equals(prefName))) {
            return null;
        }
        preference = context.getSharedPreferences(prefName, 0);
        return preference;
    }

    public static void addStringValuePref(Context context, String prefName, String key, String value)
    {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null)
        {
            SharedPreferences.Editor edit = pref.edit();
            edit.putString(key, value);
            edit.commit();
        }
    }

    public static void addBooleanValuePref(Context context, String prefName, String key, boolean value)
    {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null)
        {
            SharedPreferences.Editor edit = pref.edit();
            edit.putBoolean(key, value);
            edit.commit();
        }
    }

    public static void addFloatValuePref(Context context, String prefName, String key, float value)
    {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null)
        {
            SharedPreferences.Editor edit = pref.edit();
            edit.putFloat(key, value);
            edit.commit();
        }
    }

    public static void addLongValuePref(Context context, String prefName, String key, long value)
    {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null)
        {
            SharedPreferences.Editor edit = pref.edit();
            edit.putLong(key, value);
            edit.commit();
        }
    }

    public static void addIntValuePref(Context context, String prefName, String key, int value)
    {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null)
        {
            SharedPreferences.Editor edit = pref.edit();
            edit.putInt(key, value);
            edit.commit();
        }
    }

    public static String getStringValuePref(Context context, String prefName, String key)
    {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            return pref.getString(key, null);
        }
        return null;
    }

    public static boolean getBooleanValuePref(Context context, String prefName, String key)
    {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            return pref.getBoolean(key, false);
        }
        return false;
    }

    public static float getFloatValuePref(Context context, String prefName, String key)
    {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            return pref.getFloat(key, 0.0F);
        }
        return 0.0F;
    }

    public static long getLongValuePref(Context context, String prefName, String key)
    {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            return pref.getLong(key, 0L);
        }
        return 0L;
    }

    public static int getIntValuePref(Context context, String prefName, String key)
    {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            return pref.getInt(key, 0);
        }
        return 0;
    }

    public static void clearPref(Context context, String prefName)
    {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null)
        {
            SharedPreferences.Editor edit = pref.edit();
            edit.clear();
            edit.commit();
        }
    }
    public static void setStringGlobal(Context context, String prefName, String key, String value)
    {
        SharedPreferences pref = context.getSharedPreferences(prefName,
                Context.MODE_WORLD_READABLE);
        if (pref != null)
        {
            SharedPreferences.Editor edit = pref.edit();
            edit.putString(key, value);
            edit.commit();
        }
    }


    public static void addTableNoPref(Context context, String value)
    {

        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";

//        File file = new File(path);
//        if(!file.exists())
//            file.mkdirs();



        File savefile = new File(path + "TableNo.txt");
        LogUtil.d("테이블 번호 저장 시작 : " + savefile.getPath());
        BufferedWriter fw = null;

        try
        {
            fw = new BufferedWriter(new FileWriter(savefile, false));
            fw.write(value);
            LogUtil.d("테이블 번호 저장 완료 : " + fw);

        }
        catch (IOException e)
        {
            LogUtil.d("테이블 번호 저장 실패 : " + e);

        }
        // close file.
        if (fw != null) {
            // catch Exception here or throw.
            try {
                fw.close() ;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}

