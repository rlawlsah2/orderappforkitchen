package com.cdef.orderappforkitchen.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by kimjinmo on 2017. 9. 20..
 */

public class Utils {

    public static String getDate()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        return sdf.format(new Date());
    }

    public static String getDate(String format)
    {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date());
    }

    public static String getTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("aa hh:mm", Locale.US);
        return sdf.format(new Date());
    }

    public static String compareTime(String time)
    {
        long diff = 0;
        SimpleDateFormat currentTimeFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        try {
            Date inputDate = currentTimeFormat.parse(time);
            long inputTime = inputDate.getTime();

            ///현재시간
           diff = ((new Date()).getTime() - inputTime) / 60000;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        catch (NullPointerException e)
        {

        }

        return String.valueOf(diff);


    }


    public static String convertDate(String newFormat, String time)
    {
        SimpleDateFormat currentTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat returnTimeFormat = new SimpleDateFormat(newFormat);



        try {
            Date inputTime = currentTimeFormat.parse(time);
            String result = returnTimeFormat.format(inputTime);
            return result;

        } catch (ParseException e) {
            e.printStackTrace();
            return time;
        }

    }



    /**
     * dp -> pixel 변환
     * @param context
     * @param dp dp값
     * return int 픽셀로 변환된 값
     * ***/
    public static int dpToPx(Context context, int dp) {
        Resources resources = context.getResources();

        DisplayMetrics metrics = resources.getDisplayMetrics();

        float px = dp * (metrics.densityDpi / 160f);

        return (int) px;
    }
    /**
     * pixel -> dp 변환
     * @param context
     * @param px px 값
     * return int dp로 변환된 값
     * ***/
    public static int pxToDp(Context context, int px) {
        Resources resources = context.getResources();

        DisplayMetrics metrics = resources.getDisplayMetrics();

        float dp = px / (metrics.densityDpi / 160f);

        return (int) dp;

    }

    public static String setComma(int data)
    {
        int result = Integer.parseInt(data + "");
        return new java.text.DecimalFormat("#,###").format(result);
    }
    public static String setComma(String data)
    {
        int result = Integer.parseInt(data);
        return new java.text.DecimalFormat("#,###").format(result);
    }

    public static Long UnixTimeNow()
    {
        return (System.currentTimeMillis());

    }
    public static String getMD5(String str){

        String MD5 = "";

        try{

            MessageDigest md = MessageDigest.getInstance("MD5");

            md.update(str.getBytes());

            byte byteData[] = md.digest();

            StringBuffer sb = new StringBuffer();

            for(int i = 0 ; i < byteData.length ; i++){

                sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));

            }

            MD5 = sb.toString();



        }catch(NoSuchAlgorithmException e){

            e.printStackTrace();

            MD5 = null;

        }

//        return MD5;

        char[] string = MD5.toCharArray();
        int total = 0;
        for(int i = 0; i < 13; i++)
        {
            int tmp = Integer.parseInt(String.valueOf(string[i]), 16);
            total += tmp;
            LogUtil.d("[" + i + "] 번째 문자의 변환값 : " + tmp);

            if(total == 100)
                break;


            if(total > 100)
                total = total -100;
        }

        return total + "";


    }

    public static int getWindosWidth(Context mContext)
    {

        WindowManager.LayoutParams lp = ((Activity) mContext).getWindow().getAttributes();
        WindowManager wm = ((WindowManager) mContext.getApplicationContext().getSystemService(Context.WINDOW_SERVICE));
        return (int) (wm.getDefaultDisplay().getWidth());
    }

    public static int getWindosHeight(Context mContext)
    {

        WindowManager.LayoutParams lp = ((Activity) mContext).getWindow().getAttributes();
        WindowManager wm = ((WindowManager) mContext.getApplicationContext().getSystemService(Context.WINDOW_SERVICE));
        return (int) (wm.getDefaultDisplay().getHeight());
    }

    public static int getMD5Integer(String str){

        String MD5 = "";

        try{

            MessageDigest md = MessageDigest.getInstance("MD5");

            md.update(str.getBytes());

            byte byteData[] = md.digest();

            StringBuffer sb = new StringBuffer();

            for(int i = 0 ; i < byteData.length ; i++){

                sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));

            }

            MD5 = sb.toString();



        }catch(NoSuchAlgorithmException e){

            e.printStackTrace();

            MD5 = null;

        }

//        return MD5;

        char[] string = MD5.toCharArray();
        int total = 0;
        for(int i = 0; i < 13; i++)
        {
            int tmp = Integer.parseInt(String.valueOf(string[i]), 16);
            total += tmp;
            LogUtil.d("[" + i + "] 번째 문자의 변환값 : " + tmp);

            if(total == 100)
                break;


            if(total > 100)
                total = total -100;
        }

        return total;
    }

    public static boolean timeToShowAD(Date lastTouchTime, long interval)
    {
        long diffInMs = lastTouchTime.getTime() - Calendar.getInstance().getTime().getTime();
        long diffInSec = Math.abs(TimeUnit.MILLISECONDS.toSeconds(diffInMs));
        LogUtil.d("시간 차이 : " + diffInSec);
        return (diffInSec > interval);
    }


    // test for . $ # [ ] / and replace them
    public static String removeInvalidChars(String text) {
        text = text.replace(".", ",");
        text = text.replace("$", "@");
        text = text.replace("#", "*");
        text = text.replace("[", "(");
        text = text.replace("]", ")");
        text = text.replace("/", "backslash");
        return text;
    }

    public static String makeStackTrace(Throwable t){
        if(t == null) return "";
        try{
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            t.printStackTrace(new PrintStream(bout));
            bout.flush();
            String error = new String(bout.toByteArray());

            return error;
        }catch(Exception ex){
            return "";
        }
    }

    public static String initOrderID(String branchUid, String selectedTableNo)
    {
        String orderId = "M" + branchUid + "_" + selectedTableNo + "_" + Utils.getDate("MMddHHmmss");
        return orderId;
    }

}
