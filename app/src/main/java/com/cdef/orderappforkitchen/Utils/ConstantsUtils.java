package com.cdef.orderappforkitchen.Utils;


/**
 * Created by kimjinmo on 2016. 11. 10..
 * 앱에서 사용하는 모든 상수를 관리하는 부분
 * FILENAME이 파일단위
 * KEY는 항목단위
 *
 */

public final class ConstantsUtils {

    ///로그인 관련
    public static final String PREF_FILENAME_LOGIN = "pref_filename_login";

    /// id, pw, tableNo
    public static final String PREF_KEY_LOGIN_ID = "pref_key_login_id";
    public static final String PREF_KEY_LOGIN_PW = "pref_key_login_pw";
    public static final String PREF_KEY_LOGIN_TABLENO = "pref_key_login_talbe_no";

    //accessToken, storeBranchUid, posIp
    public static final String PREF_KEY_LOGIN_ACCESSTOKEN = "pref_key_login_accesstoken";
    public static final String PREF_KEY_LOGIN_STOREBRANCHUID = "pref_key_login_storebranchuid";
    public static final String PREF_KEY_LOGIN_STOREBRANCHID = "pref_key_login_storebranchid";
    public static final String PREF_KEY_LOGIN_POSIP = "pref_key_login_posIp";




}