package com.cdef.orderappforkitchen.Utils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.cdef.orderappforkitchen.View.Fragment.LoginFragment;
import com.cdef.orderappforkitchen.View.Fragment.MainFragment;

/**
 * Created by kimjinmo on 2016. 11. 16..
 */

public class NavigationUtils {

    public static final String BACKSTACKID = "fragments";


    public static void removeAllFragment(FragmentManager fragmentManager) {
        fragmentManager.popBackStack();
    }

    /***
     * fragment를 새로열때(단, 돌아올 때 새로 로딩 안됨) + 애니메이션 처리 없이
     * **/
    public static void detailOpenFragment(FragmentManager fragmentManager, int layoutFragment, String openFragment) {

        Fragment currentFragment = fragmentManager.findFragmentById(layoutFragment);
        Fragment checkAlreadyIncludedFragment = fragmentManager.findFragmentByTag(openFragment);

        Fragment newFragment = null;

        //열려고하는 fragment가 존재하지않으면 생성해야지
        if (checkAlreadyIncludedFragment == null) {


            if(openFragment.equals(MainFragment.class.getSimpleName()))
            {
                newFragment = new MainFragment();
            }
            else if(openFragment.equals(LoginFragment.class.getSimpleName()))
            {
                newFragment = new LoginFragment();
            }

        }

        if (currentFragment == null) //열린 화면이 하나도 없는 경우
        {
            fragmentManager.beginTransaction()
                    .add(layoutFragment, newFragment, openFragment)
                    .addToBackStack(NavigationUtils.BACKSTACKID)
                    .setTransition(FragmentTransaction.TRANSIT_NONE)
                    .commit();
        } else {
            if (checkAlreadyIncludedFragment == null)    //열린 화면은 존재하는데, 열려고 하는 화면이 manager에 없을때
            {
                removeAllFragment(fragmentManager);
                fragmentManager.beginTransaction()
                        .hide(currentFragment)
                        .add(layoutFragment, newFragment, openFragment)
                        .addToBackStack(NavigationUtils.BACKSTACKID)
                        .setTransition(FragmentTransaction.TRANSIT_NONE)
                        .commit();
            } else {
                if (currentFragment != checkAlreadyIncludedFragment)     //열린 화면이 존재하고, 열려고 하는 화면이 열린것과는 다른경우. 즉 back단에 있을때
                {
                    removeAllFragment(fragmentManager);
                    fragmentManager.beginTransaction()
                            .hide(currentFragment)
                            .add(layoutFragment, newFragment, openFragment)
                            .addToBackStack(NavigationUtils.BACKSTACKID)
                            .setTransition(FragmentTransaction.TRANSIT_NONE)
                            .commit();
                } else    //열린화면이 딱 한개인경우에 해당.
                {

                }
            }
        }
    }
}
