package com.cdef.orderappforkitchen.Repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.cdef.orderappforkitchen.DataModel.LoginData;
import com.cdef.orderappforkitchen.DataModel.OrderCategoryData;
import com.cdef.orderappforkitchen.Networking.Service;
import com.cdef.orderappforkitchen.Utils.LogUtil;
import com.cdef.orderappforkitchen.DataModel.LoginData;
import com.cdef.orderappforkitchen.DataModel.OrderCategoryData;
import com.cdef.orderappforkitchen.Networking.Service;
import com.cdef.orderappforkitchen.Utils.LogUtil;
import com.cdef.orderappforkitchen.Utils.TabletSetting;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class LoginRepository extends BaseRepository {

    public LoginRepository(Service service) {
        super(service);
    }

    MutableLiveData<LoginData> loginInfo = new MutableLiveData<>();

    public Observable<LiveData<LoginData>> getLoginInfo(String branchId, String pw) {

        ///1. 로컬에 저장된 로그인 정보가 없는지 확인할것.

        ///2. 로컬에 없다면 서버에서 가져와야한다
        return this.service.loginForManager(branchId, pw)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(loginResponse -> {
                    LogUtil.d("로그인 중 flatMap : " + (Service.requestErrorHandler(loginResponse)));
                    if (Service.requestErrorHandler(loginResponse)) {
                        loginInfo.setValue(loginResponse.result);
                    } else {
                        return Observable.error(new NullPointerException("cannot login"));
                    }
                    return Observable.just(loginInfo);
                });
    }


}
