package com.cdef.orderappforkitchen.Repository;


import com.cdef.orderappforkitchen.Networking.Service;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class BaseRepository {


    protected Service service;

    public BaseRepository(Service service)
    {
        this.service = service;
    }
    
}
