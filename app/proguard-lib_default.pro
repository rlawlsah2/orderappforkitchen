#소스 라인을 섞지않는 옵션
-keepattributes SourceFile,LineNumberTable

#소스 파일 변수 명 바꾸는 옵션
-renamesourcefileattribute SourceFile




#firebase 관련 UI라이브러리
-keep class com.androidhuman.rxfirebase2.** {*;}
-dontwarn com.androidhuman.rxfirebase2.**

-keep class com.kelvinapps.rxfirebase.** {*;}
-dontwarn com.kelvinapps.rxfirebase.**

-keep class com.github.alokagrawal8.rxfirebase.** {*;}
-dontwarn com.github.alokagrawal8.rxfirebase.**





#simpleframework
-keep public class org.simpleframework.** { *; }
-keep class org.simpleframework.xml.** { *; }
-dontwarn org.simpleframework.xml.**
-keep class org.simpleframework.xml.core.** { *; }
-keep class org.simpleframework.xml.util.** { *; }

-keepattributes ElementList, Root

-keepclassmembers class * {
    @org.simpleframework.xml.* *;
}


